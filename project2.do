onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -format Logic -radix hexadecimal /single_cycle_core_testbench/reset
add wave -noupdate -format Logic -radix hexadecimal /single_cycle_core_testbench/clk
add wave -noupdate -format Logic -radix hexadecimal /single_cycle_core_testbench/signal_bus/sig_addr_in
add wave -noupdate -format Logic -radix hexadecimal /single_cycle_core_testbench/signal_bus/sig_write_enable
add wave -noupdate -format Logic -radix hexadecimal /single_cycle_core_testbench/signal_bus/sig_write_data
add wave -noupdate -format Logic -radix hexadecimal /single_cycle_core_testbench/data_out
add wave -noupdate -format Logic -radix hexadecimal /single_cycle_core_testbench/cpu1_wait
add wave -noupdate -format Logic -radix hexadecimal /single_cycle_core_testbench/cpu1_ready
add wave -noupdate -format Logic -radix hexadecimal /single_cycle_core_testbench/cpu1_stall
add wave -noupdate -format Logic -radix hexadecimal /single_cycle_core_testbench/cpu1_fin_setup
add wave -noupdate -format Logic -radix hexadecimal /single_cycle_core_testbench/cpu2_wait
add wave -noupdate -format Logic -radix hexadecimal /single_cycle_core_testbench/cpu2_ready
add wave -noupdate -format Logic -radix hexadecimal /single_cycle_core_testbench/cpu2_stall
add wave -noupdate -format Logic -radix hexadecimal /single_cycle_core_testbench/cpu2_fin_setup
add wave -noupdate -format Literal -radix hexadecimal -expand /single_cycle_core_testbench/uut/reg_file/sig_regfile
add wave -noupdate -format Literal -radix hexadecimal /single_cycle_core_testbench/uut/sig_ID_pc
add wave -noupdate -format Literal -radix hexadecimal /single_cycle_core_testbench/uut/sig_ID_ins
add wave -noupdate -format Literal -radix hexadecimal /single_cycle_core_testbench/uut/insn_mem/sig_insn_mem
add wave -noupdate -format Literal -radix hexadecimal -expand /single_cycle_core_testbench/uut2/reg_file/sig_regfile
add wave -noupdate -format Literal -radix hexadecimal /single_cycle_core_testbench/uut2/sig_ID_pc
add wave -noupdate -format Literal -radix hexadecimal /single_cycle_core_testbench/uut2/sig_ID_ins
add wave -noupdate -format Literal -radix hexadecimal /single_cycle_core_testbench/uut2/insn_mem/sig_insn_mem
add wave -noupdate -format Literal -radix hexadecimal -expand /single_cycle_core_testbench/signal_bus/shared_memory/sig_data_mem
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {0 ps} 0}
configure wave -namecolwidth 205
configure wave -valuecolwidth 40
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
update
WaveRestoreZoom {0 ps} {3633 ns}
