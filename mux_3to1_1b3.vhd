----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    09:41:00 04/24/2018 
-- Design Name: 
-- Module Name:    mux_3to1_16b - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity mux_3to1_1b3 is
    port ( mux_select : in  std_logic(1 downto 0);
           data_a     : in  std_logic;
           data_b     : in  std_logic;
			  data_c		 : in  std_logic;
           data_out   : out std_logic);
end mux_3to1_1b3;

architecture Behavioral of mux_3to1_1b3 is
begin

    data_out <= data_a when mux_select = "00" else -- 0
                data_b when mux_select = "01" else -- 1
					 data_c when mux_select = "10" else -- 2
                'X';

end Behavioral;

