----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    16:31:55 04/27/2018 
-- Design Name: 
-- Module Name:    hazard_detection - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity hazard_detection3 is
	port( clk : in std_logic;
		  EX_mem_read	:in std_logic;
		  ID_mem_read	: in std_logic;
		  ID_RT			: in std_logic_vector(3 downto 0);
		  ID_RS			: in std_logic_vector(3 downto 0);
		  EX_RT			: in std_logic_vector(3 downto 0);
		  cpu_stall		: in std_logic;
		  stall			: out std_logic);
end hazard_detection3;

architecture Behavioral of hazard_detection3 is
signal to_stall : std_logic;
begin
	process
	begin
	wait until clk = '1' and clk'event;
		if ((ID_mem_read = '1' OR EX_mem_read = '1')  AND ((EX_RT = ID_RS) OR
			(EX_RT = ID_RT))) OR cpu_stall = '1' then
			stall <= '1';
		else
			stall <= '0';
		end if; 
	end process;
--
--	stall <= '1' when ((ID_mem_read = '1' OR EX_mem_read = '1')  AND ((EX_RT = ID_RS) OR
--			(EX_RT = ID_RT))) else
--				'0';
end Behavioral;

