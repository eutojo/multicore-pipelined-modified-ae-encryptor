----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    10:15:26 04/27/2018 
-- Design Name: 
-- Module Name:    ID_EX_reg - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity ID_EX_reg2 is
	port( 	clk					: in std_logic;
			stall				: in std_logic;
			cpu_stall			: in std_logic;
			flush				: in std_logic;
			ID_read_a			: in std_logic_vector(15 downto 0);
			ID_read_b			: in std_logic_vector(15 downto 0);
			ID_sign_ext			: in std_logic_vector(15 downto 0);
			ID_alu_ctr			: in std_logic_vector(2 downto 0);
			ID_alu_src			: in std_logic;
			ID_mem_write		: in std_logic;
			ID_mem_to_reg		: in std_logic;
			ID_reg_write		: in std_logic;
			ID_write_reg		: in std_logic_vector(3 downto 0);
			ID_npc_sel			: in std_logic_vector(1 downto 0);
			ID_RS 				: in std_logic_vector(3 downto 0);
			ID_RT				: in std_logic_vector(3 downto 0);
			ID_RD				: in std_logic_vector(3 downto 0);
			EX_read_a			: buffer std_logic_vector(15 downto 0);
			EX_read_b			: buffer std_logic_vector(15 downto 0);
			EX_sign_ext			: buffer std_logic_vector(15 downto 0);
			EX_alu_ctr			: buffer std_logic_vector(2 downto 0);
			EX_alu_src			: buffer std_logic;
			EX_mem_write		: buffer std_logic;
			EX_mem_to_reg		:buffer std_logic;
			EX_reg_write		: buffer std_logic;
			EX_write_reg		: buffer std_logic_vector(3 downto 0);
			EX_npc_sel			: buffer std_logic_vector(1 downto 0);
			EX_RS 				: buffer std_logic_vector(3 downto 0);
			EX_RT				: buffer std_logic_vector(3 downto 0);
			EX_RD				: buffer std_logic_vector(3 downto 0));
end ID_EX_reg2;

architecture Behavioral of ID_EX_reg2 is

begin

	process
	begin
		wait until clk = '1' and clk'EVENT;
		--	if cpu_stall = '0' then
				EX_read_a		<= ID_read_a;
				EX_read_b		<= ID_read_b;
				EX_sign_ext		<= ID_sign_ext;
				EX_alu_ctr		<= ID_alu_ctr;
				EX_alu_src		<= ID_alu_src;
				EX_mem_to_reg	<= ID_mem_to_reg;
				EX_write_reg	<= ID_write_reg;
				EX_RS			<= ID_RS;
				EX_RT			<= ID_RT;
				EX_RD			<= ID_RD;
				
				if flush = '1' or stall = '1' then
					EX_mem_write <= '0';
					EX_reg_write <= '0';
					EX_npc_sel		<= "00";
				else
					EX_mem_write <= ID_mem_write;
					EX_reg_write <= ID_reg_write;
					EX_npc_sel		<= ID_npc_sel;
				end if;
--			else
--				EX_read_a		<= EX_read_a;
--				EX_read_b		<= EX_read_b;
--				EX_sign_ext		<= EX_sign_ext;
--				EX_alu_ctr		<= EX_alu_ctr;
--				EX_alu_src		<= EX_alu_src;
--				EX_mem_to_reg	<= EX_mem_to_reg;
--				EX_write_reg	<= EX_write_reg;
--				EX_RS			<= EX_RS;
--				EX_RT			<= EX_RT;
--				EX_RD			<= EX_RD;
--				EX_mem_write <= EX_mem_write;
--				EX_reg_write <= EX_reg_write;
--				EX_npc_sel		<= EX_npc_sel;
--				EX_mem_write <= EX_mem_write;
--				EX_reg_write <= EX_reg_write;
--				EX_npc_sel		<= EX_npc_sel;
--				
--			end if;
		
		
	end process;
end Behavioral;

