---------------------------------------------------------------------------
-- single_cycle_core.vhd - A Single-Cycle Processor Implementation
--
-- Notes : 
--
-- See single_cycle_core.pdf for the block diagram of this single
-- cycle processor core.
--
-- Instruction Set Architecture (ISA) for the single-cycle-core:
--   Each instruction is 16-bit wide, with four 4-bit fields.
--
--     noop      
--        # no operation or to signal end of program
--        # format:  | opcode = 0 |  0   |  0   |   0    | 
--
--     load  rt, rs, offset     
--        # load data at memory location (rs + offset) into rt
--        # format:  | opcode = 1 |  rs  |  rt  | offset |
--
--     store rt, rs, offset
--        # store data rt into memory location (rs + offset)
--        # format:  | opcode = 3 |  rs  |  rt  | offset |
--
--     add   rd, rs, rt
--        # rd <- rs + rt
--        # format:  | opcode = 8 |  rs  |  rt  |   rd   |
--		 
--		 sll	rs, rd, shift
--			# rd <- rs << shift
--			# format:	| opcode = 5 |	 rs	|	rd	|  shift  |
--
--		 or	rs, rt, rd
--			# rd <- rs or rt
--			# format:	| opcode = 6 |	 rs	|	rt	|  rd  |
--
-- Copyright (C) 2006 by Lih Wen Koh (lwkoh@cse.unsw.edu.au)
-- All Rights Reserved. 
--
-- The single-cycle processor core is provided AS IS, with no warranty of 
-- any kind, express or implied. The user of the program accepts full 
-- responsibility for the application of the program and the use of any 
-- results. This work may be downloaded, compiled, executed, copied, and 
-- modified solely for nonprofit, educational, noncommercial research, and 
-- noncommercial scholarship purposes provided that this notice in its 
-- entirety accompanies all copies. Copies of the modified software can be 
-- delivered to persons who use it solely for nonprofit, educational, 
-- noncommercial research, and noncommercial scholarship purposes provided 
-- that this notice in its entirety accompanies all copies.
--
---------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
USE IEEE.STD_LOGIC_TEXTIO.ALL;
USE STD.TEXTIO.ALL;
	
entity single_cycle_core is
    port ( reset  : in  std_logic;
           clk    : in  std_logic;
           input_out : in std_logic_vector(7 downto 0);
		   ls_toggle: in std_logic;
           file_flag : out std_logic;
		   cpu_data_out: in std_logic_vector(15 downto 0);
		   cpu_write_enable: out std_logic;
		   cpu_write_data: out std_logic_vector(15 downto 0);
		   cpu_addr_in: out std_logic_vector(3 downto 0);
		   wait_signal: out std_logic;
		   ready_signal: out std_logic;
		   fin_setup: buffer std_logic;
		   fin_char	: out std_logic;
		   not_fin_char: out std_logic;
		   cpu_stall: in std_logic;
		   mem_flag: out std_logic);
		  -- cpu_src: out std_logic_vector(2 downto 0));
end single_cycle_core;

architecture structural of single_cycle_core is

component program_counter is
    port ( reset    : in  std_logic;
           clk      : in  std_logic;
           addr_in  : in  std_logic_vector(4 downto 0);
           addr_out : out std_logic_vector(4 downto 0);
           stall      : in std_logic );
end component;

component instruction_memory is
    port ( reset    : in  std_logic;
           clk      : in  std_logic;
           addr_in  : in  std_logic_vector(4 downto 0);
           insn_out : out std_logic_vector(15 downto 0) );
end component;

component sign_extend_4to16 is
    port ( data_in  : in  std_logic_vector(3 downto 0);
           data_out : out std_logic_vector(15 downto 0) );
end component;

component mux_2to1_4b is
    port ( mux_select : in  std_logic;
           data_a     : in  std_logic_vector(3 downto 0);
           data_b     : in  std_logic_vector(3 downto 0);
           data_out   : out std_logic_vector(3 downto 0) );
end component;

component mux_2to1_16b is
    port ( mux_select : in  std_logic;
           data_a     : in  std_logic_vector(15 downto 0);
           data_b     : in  std_logic_vector(15 downto 0);
           data_out   : out std_logic_vector(15 downto 0) );
end component;

component control_unit is
    port ( opcode     : in  std_logic_vector(3 downto 0);
           reg_dst    : out std_logic;
           reg_write  : out std_logic;
           alu_src    : out std_logic;
           mem_write  : out std_logic;
           mem_to_reg : out std_logic;
              alu_ctr     : out std_logic_vector(2 downto 0);
              -- flush     : out std_logic;
              npc_sel     : out std_logic_vector(1 downto 0));
end component;

component register_file is
    port ( reset           : in  std_logic;
           clk             : in  std_logic;
           read_register_a : in  std_logic_vector(3 downto 0);
           read_register_b : in  std_logic_vector(3 downto 0);
           write_enable    : in  std_logic;
           write_register  : in  std_logic_vector(3 downto 0);
           write_data      : in  std_logic_vector(15 downto 0);
           read_data_a     : out std_logic_vector(15 downto 0);
           read_data_b     : out std_logic_vector(15 downto 0) );
end component;

component adder_4b is
    port ( src_a     : in  std_logic_vector(4 downto 0);
           src_b     : in  std_logic_vector(4 downto 0);
           sum       : out std_logic_vector(4 downto 0);
		   stall	 : in std_logic;
           carry_out : out std_logic );
end component;

component adder_16b is
  port ( src_a     : in  std_logic_vector(15 downto 0);
           src_b     : in  std_logic_vector(15 downto 0);
			  alu_ctr	: in 	std_logic_vector(2 downto 0);
			  input_out : in  std_logic_vector(7 downto 0);
			  encryption_count: in std_logic_vector(4 downto 0);
           sum       : out std_logic_vector(15 downto 0);
           zero : out std_logic);
end component;

component comparator is
	port(branch_type	: in std_logic_vector(1 downto 0);
		data_a	: in std_logic_vector(15 downto 0);
		 data_b	: in std_logic_vector(15 downto 0);
		 equals	: out std_logic);
end component;

component IF_ID_reg is
    port(   clk        : in std_logic;
			stall		: in std_logic;
			cpu_stall	: in std_logic;
			flush		: in std_logic;
            IF_pc        : in std_logic_vector(4 downto 0);
            IF_ins    : in std_logic_vector(15 downto 0);
            ID_pc        : buffer std_logic_vector(4 downto 0);
            ID_ins    : buffer std_logic_vector(15 downto 0));
end component;

component ID_EX_reg is
    port( 	clk					: in std_logic;
			stall				: in std_logic;
			cpu_stall			: in std_logic;
			flush				: in std_logic;
			ID_read_a			: in std_logic_vector(15 downto 0);
			ID_read_b			: in std_logic_vector(15 downto 0);
			ID_sign_ext			: in std_logic_vector(15 downto 0);
			ID_alu_ctr			: in std_logic_vector(2 downto 0);
			ID_alu_src			: in std_logic;
			ID_mem_write		: in std_logic;
			ID_mem_to_reg		: in std_logic;
			ID_reg_write		: in std_logic;
			ID_write_reg		: in std_logic_vector(3 downto 0);
			ID_npc_sel			: in std_logic_vector(1 downto 0);
			ID_RS 				: in std_logic_vector(3 downto 0);
			ID_RT				: in std_logic_vector(3 downto 0);
			ID_RD				: in std_logic_vector(3 downto 0);
			EX_read_a			: buffer std_logic_vector(15 downto 0);
			EX_read_b			: buffer std_logic_vector(15 downto 0);
			EX_sign_ext			: buffer std_logic_vector(15 downto 0);
			EX_alu_ctr			: buffer std_logic_vector(2 downto 0);
			EX_alu_src			: buffer std_logic;
			EX_mem_write		: buffer std_logic;
			EX_mem_to_reg		: buffer std_logic;
			EX_reg_write		: buffer std_logic;
			EX_write_reg		: buffer std_logic_vector(3 downto 0);
			EX_npc_sel			: buffer std_logic_vector(1 downto 0);
			EX_RS 				: buffer std_logic_vector(3 downto 0);
			EX_RT				: buffer std_logic_vector(3 downto 0);
			EX_RD				: buffer std_logic_vector(3 downto 0));
end component;

component EX_MEM_reg is
	port(	clk					: in std_logic;
			flush					: in std_logic;
			cpu_stall 			: in std_logic;
			EX_read_b			: in std_logic_vector(15 downto 0);
			EX_mem_write		: in std_logic;
			EX_mem_to_reg		: in std_logic;
			EX_reg_write		: in std_logic;
			EX_write_reg		: in std_logic_vector(3 downto 0);
			EX_alu_ctr			: in std_logic_vector(2 downto 0);
			EX_alu_result		: in std_logic_vector(15 downto 0);
			EX_RS 				: in std_logic_vector(3 downto 0);
			EX_RT				: in std_logic_vector(3 downto 0);
			EX_RD				: in std_logic_vector(3 downto 0);
			MEM_read_b			: buffer std_logic_vector(15 downto 0);
			MEM_mem_write		: buffer std_logic;
			MEM_mem_to_reg		: buffer std_logic;
			MEM_reg_write		: buffer std_logic;
			MEM_write_reg		: buffer std_logic_vector(3 downto 0);
			MEM_alu_ctr			: buffer std_logic_vector(2 downto 0);
			MEM_alu_result		: buffer std_logic_vector(15 downto 0);
			MEM_RS 				: buffer std_logic_vector(3 downto 0);
			MEM_RT				: buffer std_logic_vector(3 downto 0);
			MEM_RD				: buffer std_logic_vector(3 downto 0));
end component;

component MEM_WB_reg is
	port( 	clk					: in std_logic;
			cpu_stall : in std_logic;
			MEM_mem_write		: in std_logic;
			MEM_mem_to_reg		: in std_logic;
			MEM_reg_write		: in std_logic;
			MEM_write_reg		: in std_logic_vector(3 downto 0);
			MEM_alu_ctr			: in std_logic_vector(2 downto 0);
			MEM_alu_result		: in std_logic_vector(15 downto 0);
			MEM_data_out		: in std_logic_vector(15 downto 0);
			MEM_RS 				: in std_logic_vector(3 downto 0);
			MEM_RT				: in std_logic_vector(3 downto 0);
			MEM_RD				: in std_logic_vector(3 downto 0);
			WB_mem_write		: buffer std_logic;
			WB_mem_to_reg		: buffer std_logic;
			WB_reg_write		: buffer std_logic;
			WB_write_reg		: buffer std_logic_vector(3 downto 0);
			WB_alu_ctr			: buffer std_logic_vector(2 downto 0);
			WB_alu_result		: buffer std_logic_vector(15 downto 0);
			WB_data_out			: buffer std_logic_vector(15 downto 0);
			WB_RS 				: buffer std_logic_vector(3 downto 0);
			WB_RT				: buffer std_logic_vector(3 downto 0);
			WB_RD				: buffer std_logic_vector(3 downto 0));
end component;

component forwarding_unit is
	port ( 	LOAD_FLAG		: in std_logic;
			EX_RS			: in std_logic_vector(3 downto 0);
			EX_RT			: in std_logic_vector(3 downto 0);
			EX_write		: in std_logic;
			MEM_write		: in std_logic;
			MEM_RS 			: in std_logic_vector(3 downto 0);
			MEM_RT			: in std_logic_vector(3 downto 0);
			MEM_RD			: in std_logic_vector(3 downto 0);
			WB_write		: in std_logic;
			WB_RD 			: in std_logic_vector(3 downto 0);
			forward_a		: out std_logic_vector(1 downto 0);
			forward_b		: out std_logic_vector(1 downto 0);
			alu_ctr			: in std_logic_vector(2 downto 0)	); --------
end component;

component hazard_detection is
	port( clk : in std_logic;
		  EX_mem_read	:in std_logic;
		  ID_mem_read	: in std_logic;
		  ID_RT			: in std_logic_vector(3 downto 0);
		  ID_RS			: in std_logic_vector(3 downto 0);
		  EX_RT			: in std_logic_vector(3 downto 0);
		  cpu_stall		: in std_logic;
		  stall			: out std_logic);
end component;

signal sig_one_4b               : std_logic_vector(4 downto 0);
signal sig_pc_carry_out         : std_logic;
signal sig_reg_dst              : std_logic;
signal sig_mem_write            : std_logic;
signal sig_mem_to_reg           : std_logic;
signal sig_write_register       : std_logic_vector(3 downto 0);
signal sig_write_data           : std_logic_vector(15 downto 0);
signal sig_read_data_b_forward           : std_logic_vector(15 downto 0);
signal sig_alu_src_b            : std_logic_vector(15 downto 0);
signal sig_data_mem_out         : std_logic_vector(15 downto 0);

signal sig_addr_4b_sum          : std_logic_vector(4 downto 0);
signal sig_register_dest       	: std_logic_vector(15 downto 0);
signal sig_to_write             : std_logic_vector(15 downto 0);
signal sig_input_out            : std_logic_vector(7 downto 0);
signal sig_file_flag            : std_logic;
signal sig_data_addr            : std_logic_vector(3 downto 0);

-- IF
signal sig_IF_pc				: std_logic_vector(4 downto 0);
signal sig_fetch_pc				: std_logic_vector(4 downto 0);
signal sig_fetch_ins			: std_logic_vector(15 downto 0);
signal sig_IF_ins				: std_logic_vector(15 downto 0);


-- ID
signal sig_ID_pc				: std_logic_vector(4 downto 0);
signal sig_ID_ins				: std_logic_vector(15 downto 0);

signal sig_ID_read_a			: std_logic_vector(15 downto 0);
signal sig_ID_read_b			: std_logic_vector(15 downto 0);
signal sig_ID_sign_ext			: std_logic_vector(15 downto 0);
signal sig_ID_alu_ctr			: std_logic_vector(2 downto 0);
signal sig_ID_alu_src			: std_logic;
signal sig_ID_mem_write			: std_logic;
signal sig_ID_mem_to_reg		: std_logic;
signal sig_ID_reg_write			: std_logic;
signal sig_ID_write_reg			: std_logic_vector(3 downto 0);
signal sig_ID_npc_sel			: std_logic_vector(1 downto 0);

signal sig_ID_branch			: std_logic;
signal comparator_value			: std_logic;

-- EX
signal sig_EX_read_a			: std_logic_vector(15 downto 0);
signal sig_EX_read_b			: std_logic_vector(15 downto 0);
signal sig_EX_sign_ext			: std_logic_vector(15 downto 0);
signal sig_EX_alu_ctr			: std_logic_vector(2 downto 0);
signal sig_EX_alu_src			: std_logic;
signal sig_EX_mem_write			: std_logic;
signal sig_EX_mem_to_reg		: std_logic;
signal sig_EX_reg_write			: std_logic;
signal sig_EX_write_reg			: std_logic_vector(3 downto 0);
signal sig_EX_npc_sel			: std_logic_vector(1 downto 0);

signal sig_EX_zero				: std_logic;
signal sig_EX_alu_result		: std_logic_vector(15 downto 0);
signal sig_immediate            : std_logic_vector(15 downto 0);

-- MEM
signal sig_MEM_read_b			: std_logic_vector(15 downto 0);
signal sig_MEM_mem_write		: std_logic;
signal sig_MEM_mem_to_reg		: std_logic;
signal sig_MEM_reg_write		: std_logic;
signal sig_MEM_write_reg		: std_logic_vector(3 downto 0);
signal sig_MEM_alu_ctr			: std_logic_vector(2 downto 0);
signal sig_MEM_alu_result		: std_logic_vector(15 downto 0);
signal sig_MEM_data_out			: std_logic_vector(15 downto 0);

-- WB
signal sig_WB_mem_write		: std_logic;
signal sig_WB_mem_to_reg	: std_logic;
signal sig_WB_reg_write		: std_logic;
signal sig_WB_write_reg		: std_logic_vector(3 downto 0);
signal sig_WB_alu_ctr		: std_logic_vector(2 downto 0);
signal sig_WB_alu_result	: std_logic_vector(15 downto 0);
signal sig_WB_data_out		: std_logic_vector(15 downto 0);

signal sig_WB_write_data	: std_logic_vector(15 downto 0);

-- FORWARDING UNITS
signal sig_forward_a		: std_logic_vector(1 downto 0);
signal sig_forward_b		: std_logic_vector(1 downto 0);
signal sig_forward_a_dummy		: std_logic_vector(1 downto 0);
signal sig_forward_b_dummy		: std_logic_vector(1 downto 0);
signal alu_data_a			: std_logic_vector(15 downto 0);
signal alu_data_b			: std_logic_vector(15 downto 0);
signal sig_ID_RS				: std_logic_vector(3 downto 0);
signal sig_ID_RT				: std_logic_vector(3 downto 0);
signal sig_ID_RD				: std_logic_vector(3 downto 0);
signal sig_EX_RS				: std_logic_vector(3 downto 0);
signal sig_EX_RT				: std_logic_vector(3 downto 0);
signal sig_EX_RD				: std_logic_vector(3 downto 0);
signal sig_MEM_RS				: std_logic_vector(3 downto 0);
signal sig_MEM_RT				: std_logic_vector(3 downto 0);
signal sig_MEM_RD				: std_logic_vector(3 downto 0);
signal sig_WB_RS				: std_logic_vector(3 downto 0);
signal sig_WB_RT				: std_logic_vector(3 downto 0);
signal sig_WB_RD				: std_logic_vector(3 downto 0);
signal sig_WB_check			: std_logic_vector(3 downto 0);
signal sig_RD_check			: std_logic_vector(3 downto 0);

-- HAZARD DETECTION
signal sig_stall				: std_logic;
signal hazard_EX_mem_write		: std_logic;
signal hazard_EX_reg_write		: std_logic;
signal sig_ls_flag				: std_logic;
signal sig_a_value				: std_logic_vector(15 downto 0);
signal sig_b_value				: std_logic_vector(15 downto 0);
signal sig_delay_branch			: std_logic_vector(1 downto 0);
signal sig_hazard_ID_RD_check			: std_logic_vector(3 downto 0);
signal sig_hazard_EX_RD_check			: std_logic_vector(3 downto 0);

-- FOR LS
signal sig_reg_dst_out		: std_logic_vector(3 downto 0);

-- for branching
signal sig_ex_branch			: std_logic;
signal sig_branch_address	: std_logic_vector(4 downto 0);
signal sig_jump_address	: std_logic_vector(3 downto 0);
signal sig_cpi          : std_logic_vector(15 downto 0);

-- shared memory
signal sig_cpu_stall : std_logic;
signal sig_cpu_ready : std_logic := '1';
SIGNAL cc_count: std_logic_vector(15 downto 0) := (OTHERS => '0');
signal setup_flag : std_logic;

-- for encryption
signal sig_encryption_count : std_logic_vector(4 downto 0);
signal sig_new_encryption_count: std_logic_vector(4 downto 0);

begin

     sig_one_4b <= "00001"; 
    
     -- just counts the number of clock cycles that have passed	
	 PROCESS
		  BEGIN
			WAIT UNTIL clk'EVENT and CLK = '1' AND RESET = '0';
				cc_count <= cc_count + 1;
		  END PROCESS;
	
	-- signal raised when its attemping to fetch a character from testbench
    file_flag <= '1' when sig_EX_alu_ctr = "010" else '0';
	
     -- ##################################################                  
     -- ################# IF STAGE #######################
	 -- ##################################################
	 
	-- signal to inform the hazard detection unit to stall
	sig_cpu_stall <= cpu_stall;
	
    pc : program_counter
    port map ( reset    => reset,
               clk      => clk,
               addr_in  => sig_addr_4b_sum,
               addr_out => sig_fetch_pc,
               stall     => sig_stall); 
		   
	next_pc : adder_4b
    port map ( src_a     => sig_IF_pc, 
               src_b     => sig_one_4b,
               sum       => sig_addr_4b_sum,
			   stall     => sig_stall,
               carry_out => sig_pc_carry_out );
    
    insn_mem : instruction_memory 
    port map ( reset    => reset,
               clk      => clk,
               addr_in  => sig_IF_pc,
               insn_out => sig_IF_ins );
	
	-- IF conditions: keep the same when stalling, go to the branch address
	--	when branching is detected, else go to the next instruction
	sig_IF_pc <= sig_IF_pc when sig_stall = '1' else 
				sig_branch_address when sig_ex_branch = '1' else
				sig_fetch_pc;
					
	-- READY -> next person can use memory [5000]
	ready_signal <= '1' when sig_MEM_alu_ctr = "110" AND (sig_MEM_rt = "0000") else
					'0';
	
	-- WAIT -> waiting for memory access
	wait_signal <= '1' when sig_IF_ins(15 downto 12) = "0100" else '0';
	
	-- READY -> character has been encrypted [5001]
	fin_char <= '1' when sig_MEM_alu_ctr = "110" AND (sig_MEM_rd = "0001") else '0';
	
	-- tells whether cpu1 will wait for cpu2 to finish or cpu1 will get next char
	not_fin_char <= '1' when sig_MEM_alu_ctr = "110" AND (sig_MEM_rd = "0001") AND sig_encryption_count > 4 else '0';
	
	-- READ -> next character can write [5100]
	fin_setup <= '1' when sig_MEM_alu_ctr = "110" AND (sig_MEM_rs = "0001") else '0';
			
	IF_ID_pipeline_reg : IF_ID_reg
	port map(	clk		=> clk,
				stall	=> sig_stall,
				cpu_stall => cpu_stall,
				flush	=> sig_ex_branch,
				IF_pc	=> sig_IF_pc,
				IF_ins	=> sig_IF_ins ,
				ID_pc	=> sig_ID_pc,
				ID_ins	=> sig_ID_ins);

     -- ##################################################                  
     -- ################# ID STAGE #######################
	 -- ##################################################
    sign_extend : sign_extend_4to16 
    port map ( data_in  => sig_ID_ins(3 downto 0),
               data_out => sig_ID_sign_ext);

    ctrl_unit : control_unit 
    port map ( opcode     => sig_ID_ins(15 downto 12),
               reg_dst    => sig_reg_dst,
               reg_write  => sig_ID_reg_write,
               alu_src    => sig_ID_alu_src,
               mem_write  => sig_ID_mem_write,
               mem_to_reg => sig_ID_mem_to_reg,
               alu_ctr    => sig_ID_alu_ctr,
               npc_sel    => sig_ID_npc_sel);
				
    mux_reg_dst : mux_2to1_4b 
    port map ( mux_select => sig_reg_dst,
               data_a     => sig_ID_ins(7 downto 4),
               data_b     => sig_ID_ins(3 downto 0),
               data_out   => sig_ID_write_reg);
	
    reg_file : register_file 
    port map ( reset           => reset, 
               clk             => clk,
               read_register_a => sig_ID_ins(11 downto 8),
               read_register_b => sig_ID_ins(7 downto 4),
               write_enable    => sig_WB_reg_write,
               write_register  => sig_WB_write_reg,
               write_data      => sig_WB_write_data,
               read_data_a     => sig_ID_read_a,
               read_data_b     => sig_ID_read_b );
	
	-- some forwarding issue
	sig_read_data_b_forward <= sig_MEM_alu_result when (sig_ID_RT = sig_MEM_rd) AND (sig_ID_mem_write = '1' AND sig_MEM_reg_write = '1') else sig_ID_read_b;
	
	-- ls_toggle = '1' when it is using CPU2's data ie. when reusing key
	sig_ID_RS <= "0000" when (sig_ID_RT = "0100" AND sig_ID_ins(15 downto 12) = "0001") else
					sig_ID_ins(11 downto 8);
	sig_ID_RT <= sig_ID_ins(7 downto 4);
	sig_ID_RD <= sig_ID_ins(11 downto 8) when (ls_toggle = '1' AND sig_ID_RT = "0100" AND sig_ID_ins(15 downto 12) = "0001") else 
					sig_ID_ins(3 downto 0);
	
	-- ls is to be treated like a mem-to-reg operation
	sig_ls_flag <= '1' when sig_ID_ins(15 downto 12) = "1101" else '0';
	
	hazard_detection_unit : hazard_detection
	port map( clk => clk, 
			  EX_mem_read	=> sig_ID_mem_to_reg,
			  ID_mem_read	=> sig_ls_flag,
			  ID_RT			=> sig_IF_ins(7 downto 4),
			  ID_RS			=> sig_IF_ins(11 downto 8),
			  EX_RT			=> sig_ID_RT,
			  cpu_stall 	=> cpu_stall,
			  stall			=> sig_stall);
	
	ID_EX_pipeline_reg : ID_EX_reg
	port map (	clk					=> clk,
				stall				=> sig_stall,
				cpu_stall			=> sig_cpu_stall,
				flush				=> sig_ex_branch,
				ID_read_a			=> sig_ID_read_a,
				ID_read_b			=> sig_read_data_b_forward,
				ID_sign_ext			=> sig_ID_sign_ext,
				ID_alu_ctr			=> sig_ID_alu_ctr,
				ID_alu_src			=> sig_ID_alu_src,
				ID_mem_write		=> sig_ID_mem_write,
				ID_mem_to_reg		=> sig_ID_mem_to_reg,
				ID_reg_write		=> sig_ID_reg_write,
				ID_write_reg		=> sig_ID_write_reg,
				ID_npc_sel			=> sig_ID_npc_sel,
				ID_RS					=> sig_ID_RS,
				ID_RT					=> sig_ID_RT,
				ID_RD					=> sig_ID_RD,
				EX_read_a			=> sig_EX_read_a,
				EX_read_b			=> sig_EX_read_b,
				EX_sign_ext			=> sig_EX_sign_ext,
				EX_alu_ctr			=> SIG_EX_alu_ctr,
				EX_alu_src			=> sig_EX_alu_src,
				EX_mem_write		=> sig_EX_mem_write,
				EX_mem_to_reg		=> sig_EX_mem_to_reg,
				EX_reg_write		=> sig_EX_reg_write,
				EX_write_reg		=> sig_EX_write_reg,
				EX_npc_sel			=> sig_EX_npc_sel,
				EX_RS				=> sig_EX_RS,
				EX_RT				=> sig_EX_RT,
				EX_RD				=> sig_EX_RD);

     -- ##################################################                  
     -- ################# EX STAGE #######################
	 -- ##################################################	
    
    mux_alu_src : mux_2to1_16b 
    port map ( mux_select => sig_EX_alu_src,
               data_a     => sig_EX_read_b,
               data_b     => sig_EX_sign_ext,
               data_out   => sig_alu_src_b );
			   
	-- keep in mind, the destination registers for i-type instructions are different to r-type instructions
	-- ie. for i-type: rs | rd | offset and
	-- 	   for r-type: rs | rt | rd
	-- note: this is different from what is up top because rd is interpreted as the destination register
	sig_wb_check <= sig_WB_RT when (sig_WB_mem_to_reg = '1') else sig_WB_RD;
		sig_RD_check <= sig_MEM_RT when (sig_MEM_mem_to_reg = '1') else sig_MEM_RD;
	
	data_forwarding : forwarding_unit
	port map ( LOAD_FLAG	=> sig_MEM_mem_to_reg,
				EX_RS      => sig_EX_RS,
			   EX_RT	  => sig_EX_RT,
			   EX_write	  => sig_MEM_mem_to_reg,
			   MEM_write  => sig_MEM_reg_write,
			   MEM_RS	  => sig_MEM_RS,
			   MEM_RT	  => sig_MEM_RT,
			   MEM_RD 	  => sig_RD_check,
			   WB_write	  => sig_WB_reg_write,
			   WB_RD 	  => sig_wb_check,
			   forward_a  => sig_forward_a,
			   forward_b  => sig_forward_b,
				alu_ctr 	  => sig_EX_alu_ctr	);  
	
	-- comparing results between having and not having a forwarding unit
	-- sig_forward_a <= "00";
	-- sig_forward_b <= "00";
	alu_data_a <= sig_MEM_alu_result when sig_forward_a = "10" else
				  sig_WB_write_data when (sig_forward_a = "01") else
				  sig_EX_read_a;
	alu_data_b <= sig_MEM_alu_result when sig_forward_b = "10" else --make sure not shift
				  sig_WB_write_data when sig_forward_b = "01" else
				  sig_immediate;
	
	-- for immediate instructions (subi, not sure why the sign extend wont work)
	sig_immediate <= "000000000000" & sig_EX_rt when sig_EX_alu_ctr = "101" else sig_alu_src_b;
	
	-- original encryption count obtained from memory, other times, after a sub2
	-- only change when these two are changed
	sig_encryption_count <= sig_MEM_data_out(4 downto 0) when sig_MEM_rt = "0100" AND sig_MEM_mem_to_reg = '1' else 
								sig_EX_alu_result(4 downto 0) when sig_EX_rd = "0100"  AND sig_EX_alu_ctr = "101" else
								sig_encryption_count;
	
	alu : adder_16b 
    port map ( src_a    	=> alu_data_a,
               src_b     	=> alu_data_b,
               alu_ctr		=> sig_EX_alu_ctr,
               input_out	=> input_out,
			   encryption_count => sig_encryption_count,
               sum       	=> sig_EX_alu_result,
               zero 		=> sig_EX_zero);
	
	--  comparing with an immediate value
	sig_cpi <= "000000000000" & sig_EX_RT when sig_EX_npc_sel = "01" else alu_data_b;
	
	compare_data : comparator
	port map(branch_type => sig_EX_npc_sel, 
		 data_a	=> alu_data_a,
		 data_b	=> sig_cpi,
		 equals	=> comparator_value);
	
	-- signal to branch
	sig_ex_branch <= '1' when comparator_value = '1' else '0';
	
	-- calculation of  branch address -> last 4 bits when BNE or BEQ, last 5 bits when JMP
	sig_branch_address <= (sig_EX_RT(0) & sig_EX_RD) when sig_EX_npc_sel = "10" else '0' & sig_EX_RD;
			
	EX_MEM_pipeline_reg : EX_MEM_reg
	port map(	clk					=> clk,
				flush					=> sig_ex_branch,
				cpu_stall			=> sig_cpu_stall,
				EX_read_b			=> sig_EX_read_b,
				EX_mem_write		=> sig_EX_mem_write,
				EX_mem_to_reg		=> sig_EX_mem_to_reg,
				EX_reg_write		=> sig_EX_reg_write,
				EX_write_reg		=> sig_EX_write_reg,
				EX_alu_ctr			=> sig_EX_alu_ctr,
				EX_alu_result		=> sig_EX_alu_result,
				EX_RS					=> sig_EX_RS,
				EX_RT					=> sig_EX_RT,
				EX_RD					=> sig_EX_RD,
				MEM_read_b			=> sig_MEM_read_b,
				MEM_mem_write		=> sig_MEM_mem_write,
				MEM_mem_to_reg		=> sig_MEM_mem_to_reg,
				MEM_reg_write		=> sig_MEM_reg_write,
				MEM_write_reg		=> sig_MEM_write_reg,
				MEM_alu_ctr			=> sig_MEM_alu_ctr,
				MEM_alu_result		=> sig_MEM_alu_result,
				MEM_RS				=> sig_MEM_RS,
				MEM_RT				=> sig_MEM_RT,
				MEM_RD				=> sig_MEM_RD);
	
	 -- ##################################################                  
     -- ################ MEM STAGE #######################
	 -- ##################################################

	-- separate condition for mem_write (sometimes it is wrong?) as well as LS
	sig_reg_dst_out <= sig_MEM_rd when sig_MEM_mem_write = '1' else
						sig_MEM_rt when sig_MEM_alu_ctr = "001" else
						sig_MEM_alu_result(3 downto 0);
    
	-- for loading the character 
	sig_write_data <= sig_MEM_alu_result when sig_MEM_alu_ctr = "001" else sig_MEM_read_b;
	
	-- data memory was extracted from here
    sig_MEM_data_out <= cpu_data_out;
    cpu_write_enable <= sig_MEM_mem_write;
    cpu_write_data <= sig_write_data;
    cpu_addr_in <= sig_reg_dst_out;
	
	-- testing purpose: used to signal that the core is using memory
	mem_flag <= '1' WHEN (sig_MEM_mem_write = '1' OR sig_WB_reg_write = '1') else '0';
	
	-- separate condition for when core 1 needs to obtain the character from memory rather than testbench
	sig_mem_to_reg <= '1' WHEN ls_toggle = '1' AND sig_MEM_alu_ctr = "001" else sig_MEM_mem_to_reg;
	
	
	MEM_WB_pipeline_reg	: MEM_WB_reg
	port map( 	clk					=> clk,
				cpu_stall			=> sig_cpu_stall,
				MEM_mem_write		=> sig_mem_write,
				MEM_mem_to_reg		=> sig_mem_to_reg,
				MEM_reg_write		=> sig_MEM_reg_write,
				MEM_write_reg		=> sig_MEM_write_reg,
				MEM_alu_ctr			=> sig_MEM_alu_ctr,
				MEM_alu_result		=> sig_MEM_alu_result,
				MEM_data_out		=> sig_MEM_data_out,
				MEM_RS				=> sig_MEM_RS,
				MEM_RT				=> sig_MEM_RT,
				MEM_RD				=> sig_MEM_RD,
				WB_mem_write		=> sig_WB_mem_write,
				WB_mem_to_reg		=> sig_WB_mem_to_reg,
				WB_reg_write		=> sig_WB_reg_write,
				WB_write_reg		=> sig_WB_write_reg,
				WB_alu_ctr			=> sig_WB_alu_ctr,
				WB_alu_result		=> sig_WB_alu_result,
				WB_data_out			=> sig_WB_data_out,
				WB_RS				=> sig_WB_RS,
				WB_RT				=> sig_WB_RT,
				WB_RD				=> sig_WB_RD);
	
	 -- ##################################################                  
     -- ################# WB STAGE #######################
	 -- ##################################################
	
    mux_mem_to_reg : mux_2to1_16b 
    port map ( mux_select => sig_WB_mem_to_reg,
               data_a     => sig_WB_alu_result,
               data_b     => sig_WB_data_out,
               data_out   => sig_WB_write_data);
			   

end structural;
