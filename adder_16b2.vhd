---------------------------------------------------------------------------
-- adder_16b.vhd - 16-bit Adder Implementation
--
--
-- Copyright (C) 2006 by Lih Wen Koh (lwkoh@cse.unsw.edu.au)
-- All Rights Reserved. 
--
-- The single-cycle processor core is provided AS IS, with no warranty of 
-- any kind, express or implied. The user of the program accepts full 
-- responsibility for the application of the program and the use of any 
-- results. This work may be downloaded, compiled, executed, copied, and 
-- modified solely for nonprofit, educational, noncommercial research, and 
-- noncommercial scholarship purposes provided that this notice in its 
-- entirety accompanies all copies. Copies of the modified software can be 
-- delivered to persons who use it solely for nonprofit, educational, 
-- noncommercial research, and noncommercial scholarship purposes provided 
-- that this notice in its entirety accompanies all copies.
--
---------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
--use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.NUMERIC_STD.ALL;

entity adder_16b2 is
    port ( src_a     : in  std_logic_vector(15 downto 0);
           src_b     : in  std_logic_vector(15 downto 0);
			  alu_ctr	: in 	std_logic_vector(2 downto 0);
			  encryption_count: in std_logic_vector(4 downto 0);
           sum       : out std_logic_vector(15 downto 0);
           zero : out std_logic);
end adder_16b2;

architecture behavioural of adder_16b2 is

	component rn_table2 is
		port(to_encode : in std_logic_vector(15 downto 0);
		  encrypt_key : in std_logic_vector(7 downto 0);
		  encryption_count : in std_logic_vector(4 downto 0);
		  encoded	 : out std_logic_vector(15 downto 0));
	end component;
	
signal sig_result : std_logic_vector(16 downto 0);
signal srl_result : std_logic_vector(15 downto 0);
signal sll_result : std_logic_vector(15 downto 0);
signal xor_result : std_logic_vector(15 downto 0);
signal xor_encrypt : std_logic_vector(15 downto 0);
signal sig_encrypt_result : std_logic_vector(15 downto 0);
signal sig_xor			: std_logic_vector(15 downto 0);
signal sig_new_encryption_count : std_logic_vector(4 downto 0);

constant ALU_SLLV : std_logic_vector(2 downto 0) := "010";
constant ALU_XOR  : std_logic_vector(2 downto 0) := "011";
constant ALU_SRLV  : std_logic_vector(2 downto 0) := "100";
constant ALU_SUB	: std_logic_vector(2 downto 0) := "101"; 
constant ALU_AND	: std_logic_vector(2 downto 0) := "111"; 


begin

	 -- only subtraction, min value is 0
	sig_result <= ('0' & src_a) - ('0' & src_b) when (alu_ctr = ALU_SUB AND src_a > src_b) else
					"00000000000000000"
	 
	 -- first look up
	 rn_lookup1: rn_table2
	 port map (to_encode => src_a,
				  encrypt_key => src_b(15 downto 8),
				  encryption_count => encryption_count,
					encoded => xor_encrypt);
					
	 -- update number of required encryptions
	 sig_new_encryption_count <= encryption_count-1 when encryption_count > "00000" else "00000";
	
	 -- second lookup
	 rn_lookup2: rn_table2
	 port map (to_encode => xor_encrypt,
					encrypt_key => src_b(7 downto 0),
					encryption_count => sig_new_encryption_count,
					encoded => sig_encrypt_result);
	 
	 -- shifting results
	 srl_result <= to_stdlogicvector(to_bitvector(src_a) srl (to_integer(unsigned(src_b(3 downto 0)))));
	 sll_result <= to_stdlogicvector(to_bitvector(src_a) sll (to_integer(unsigned(src_b(3 downto 0)))));

	 -- result of ALU	 
	 sum <= 	srl_result when alu_ctr = ALU_SRLV else
				sll_result when alu_ctr = ALU_SLL) else
				sig_encrypt_result when (alu_ctr = ALU_XOR) else
				sig_result(15 downto 0);
				
	 -- not used	 
	 zero <= (src_a(15) xnor src_b(15)) AND (src_a(14) xnor src_b(14)) AND (src_a(13) xnor src_b(13)) AND
				(src_a(12) xnor src_b(12)) AND (src_a(11) xnor src_b(11)) AND (src_a(10) xnor src_b(10)) AND
				(src_a(9) xnor src_b(9)) AND (src_a(8) xnor src_b(8)) AND (src_a(7) xnor src_b(7)) AND
				(src_a(6) xnor src_b(6)) AND (src_a(5) xnor src_b(5)) AND (src_a(4) xnor src_b(4)) AND
				(src_a(3) xnor src_b(3)) AND (src_a(2) xnor src_b(2)) AND (src_a(1) xnor src_b(1)) AND
				(src_a(0) xnor src_b(0));
    
end behavioural;
