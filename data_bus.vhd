----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    20:47:52 05/14/2018 
-- Design Name: 
-- Module Name:    bus - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity data_bus is
	port( reset : in std_logic;
		  clk : in std_logic;
		  fin_setup1	: in std_logic;
		  write_enable1 : in  std_logic;
          write_data1   : in  std_logic_vector(15 downto 0);
          addr_in1      : in  std_logic_vector(3 downto 0);
		  fin_setup2	: in std_logic;
		  write_enable2 : in  std_logic;
          write_data2   : in  std_logic_vector(15 downto 0);
          addr_in2      : in  std_logic_vector(3 downto 0);
		  fin_setup3	: in std_logic;
		  write_enable3 : in  std_logic;
          write_data3   : in  std_logic_vector(15 downto 0);
          addr_in3      : in  std_logic_vector(3 downto 0);
		  fin_setup4	: in std_logic;
		  write_enable4 : in  std_logic;
          write_data4   : in  std_logic_vector(15 downto 0);
          addr_in4      : in  std_logic_vector(3 downto 0);
          data_out     : out std_logic_vector(15 downto 0)
	);
end data_bus;

architecture Behavioral of data_bus is

	component data_memory is
		port ( reset        : in  std_logic;
           clk          : in  std_logic;
           write_enable : in  std_logic;
           write_data   : in  std_logic_vector(15 downto 0);
           addr_in      : in  std_logic_vector(3 downto 0);
           data_out     : out std_logic_vector(15 downto 0));
	end component;
		
	signal		sig_write_enable :   std_logic;
    signal       sig_write_data   :   std_logic_vector(15 downto 0);
    signal       sig_addr_in      :   std_logic_vector(3 downto 0);

BEGIN

	sig_write_enable <= write_enable1 when fin_setup1 = '1' else
						write_enable2 when fin_setup2 = '1' else
						write_enable3 when fin_setup3 = '1' else
						write_enable4 when fin_setup4 = '1' else
						'0';
						
	sig_write_data <= write_data1 when fin_setup1 = '1' else
						write_data2 when fin_setup2 = '1' else
						write_data3 when fin_setup3 = '1' else
						write_data4;

	sig_addr_in <= addr_in1 when fin_setup1 = '1' else
						addr_in2 when fin_setup2 = '1' else
						addr_in3 when fin_setup3 = '1' else
						addr_in4;

	shared_memory: data_memory
	PORT MAP( reset => reset,
			  clk => clk,
			  write_enable => sig_write_enable,
			  write_data => sig_write_data,
			addr_in => sig_addr_in,
			data_out => data_out
	);
		
end Behavioral;

