----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    16:31:27 04/27/2018 
-- Design Name: 
-- Module Name:    forwarding_unit - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity forwarding_unit2 is
	port ( 	LOAD_FLAG		: in std_logic;
			EX_RS			: in std_logic_vector(3 downto 0);
			EX_RT			: in std_logic_vector(3 downto 0);
			EX_write		: in std_logic;
			MEM_write		: in std_logic;
			MEM_RS 			: in std_logic_vector(3 downto 0);
			MEM_RT			: in std_logic_vector(3 downto 0);
			MEM_RD			: in std_logic_vector(3 downto 0);
			WB_write		: in std_logic;
			WB_RD 			: in std_logic_vector(3 downto 0);
			forward_a		: out std_logic_vector(1 downto 0);
			forward_b		: out std_logic_vector(1 downto 0);
			alu_ctr			: in std_logic_vector(2 downto 0)	);
end forwarding_unit2;

architecture Behavioral of forwarding_unit2 is
signal sig_RD_compare : std_logic_vector(3 downto 0);
signal sig_forward_a  : std_logic_vector(1 downto 0);
signal sig_forward_b  : std_logic_vector(1 downto 0);

begin

	-- ### NOTE ###
	-- 10 : data taken from earlier ALU calculation
	-- 01 : data taken from WB stage
	
	-- ## DATA A : RS ##
	sig_forward_a <= "10" when (MEM_write = '1' AND (MEM_RD /= "0000") AND  (MEM_RD = EX_RS)) else
	
					 "01" when (WB_write = '1' AND (WB_RD /= "0000") AND
								(WB_RD = EX_RS)) else
					 "00";
	
	-- ## DATA B : RT ##
	sig_forward_b <= "10" when ((MEM_write = '1') AND (MEM_RD /= "0000") AND  (MEM_RD = EX_RT)) else
	
				 "01" when (WB_write = '1' AND (WB_RD /= "0000") AND
								(WB_RD = EX_RT)) else
				"00";
	forward_a <= sig_forward_a;
	forward_b <= sig_forward_b;
	
end Behavioral;

