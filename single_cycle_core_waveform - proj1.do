onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -format Logic -radix hexadecimal /single_cycle_core_testbench/reset
add wave -noupdate -format Logic -radix hexadecimal /single_cycle_core_testbench/clk
add wave -noupdate -format Logic -radix hexadecimal /single_cycle_core_testbench/uut/cc_count
add wave -noupdate -format Literal -radix unsigned /single_cycle_core_testbench/uut/sig_addr_4b_sum
add wave -noupdate -format Literal -radix hexadecimal /single_cycle_core_testbench/uut/sig_stall
add wave -noupdate -format Literal -radix hexadecimal /single_cycle_core_testbench/uut/sig_file_flag;
add wave -noupdate -format Literal -radix hexadecimal /single_cycle_core_testbench/uut/ls_addr;
add wave -noupdate -format Literal -radix hexadecimal /single_cycle_core_testbench/uut/sig_early_branch;
add wave -noupdate -format Literal -radix hexadecimal /single_cycle_core_testbench/uut/sig_jump_address;
add wave -noupdate -format Literal -radix hexadecimal /single_cycle_core_testbench/uut/sig_ex_branch
add wave -noupdate -format Literal -radix hexadecimal /single_cycle_core_testbench/uut/sig_branch_address;
add wave -noupdate -format Literal -radix hexadecimal /single_cycle_core_testbench/uut/sig_IF_ins
add wave -noupdate -format Literal -radix hexadecimal /single_cycle_core_testbench/uut/sig_IF_pc
add wave -noupdate -format Literal -radix hexadecimal /single_cycle_core_testbench/uut/sig_ID_pc
add wave -noupdate -format Literal -radix hexadecimal /single_cycle_core_testbench/uut/sig_ID_ins
add wave -noupdate -format Literal -radix hexadecimal /single_cycle_core_testbench/uut/sig_ID_RS
add wave -noupdate -format Literal -radix hexadecimal /single_cycle_core_testbench/uut/sig_ID_RT
add wave -noupdate -format Literal -radix hexadecimal /single_cycle_core_testbench/uut/sig_ID_RD
add wave -noupdate -format Literal -radix hexadecimal /single_cycle_core_testbench/uut/sig_ID_read_a
add wave -noupdate -format Literal -radix hexadecimal /single_cycle_core_testbench/uut/sig_ID_read_b
add wave -noupdate -format Literal -radix hexadecimal /single_cycle_core_testbench/uut/sig_ID_sign_ext
add wave -noupdate -format Literal -radix unsigned /single_cycle_core_testbench/uut/sig_ID_alu_ctr
add wave -noupdate -format Logic -radix hexadecimal /single_cycle_core_testbench/uut/sig_ID_alu_src
add wave -noupdate -format Logic -radix hexadecimal /single_cycle_core_testbench/uut/sig_ID_mem_to_reg
add wave -noupdate -format Literal -radix hexadecimal /single_cycle_core_testbench/uut/sig_ID_write_reg
add wave -noupdate -format Literal -radix hexadecimal /single_cycle_core_testbench/uut/sig_ID_npc_sel
add wave -noupdate -format Literal -radix hexadecimal /single_cycle_core_testbench/uut/sig_EX_RS
add wave -noupdate -format Literal -radix hexadecimal /single_cycle_core_testbench/uut/sig_EX_RT
add wave -noupdate -format Literal -radix hexadecimal /single_cycle_core_testbench/uut/sig_EX_RD
add wave -noupdate -format Literal -radix hexadecimal /single_cycle_core_testbench/uut/alu_data_a
add wave -noupdate -format Literal -radix hexadecimal /single_cycle_core_testbench/uut/alu_data_b
add wave -noupdate -format Literal -radix hexadecimal /single_cycle_core_testbench/uut/comparator_value
add wave -noupdate -format Literal -radix hexadecimal /single_cycle_core_testbench/uut/sig_EX_read_b
add wave -noupdate -format Literal -radix hexadecimal /single_cycle_core_testbench/uut/sig_EX_sign_ext
add wave -noupdate -format Literal -radix unsigned /single_cycle_core_testbench/uut/sig_EX_alu_ctr
add wave -noupdate -format Logic -radix hexadecimal /single_cycle_core_testbench/uut/sig_EX_alu_src
add wave -noupdate -format Logic -radix hexadecimal /single_cycle_core_testbench/uut/sig_EX_mem_write
add wave -noupdate -format Logic -radix hexadecimal /single_cycle_core_testbench/uut/sig_EX_mem_to_reg
add wave -noupdate -format Logic -radix hexadecimal /single_cycle_core_testbench/uut/sig_EX_reg_write
add wave -noupdate -format Literal -radix hexadecimal /single_cycle_core_testbench/uut/sig_EX_write_reg
add wave -noupdate -format Literal -radix hexadecimal /single_cycle_core_testbench/uut/sig_EX_npc_sel
add wave -noupdate -format Logic -radix hexadecimal /single_cycle_core_testbench/uut/sig_EX_zero
add wave -noupdate -format Literal -radix hexadecimal /single_cycle_core_testbench/uut/sig_EX_alu_result
add wave -noupdate -format Literal -radix binary /single_cycle_core_testbench/uut/sig_forward_a
add wave -noupdate -format Literal -radix binary /single_cycle_core_testbench/uut/sig_forward_b
add wave -noupdate -format Literal -radix hexadecimal /single_cycle_core_testbench/uut/alu_data_a
add wave -noupdate -format Literal -radix hexadecimal /single_cycle_core_testbench/uut/alu_data_b
add wave -noupdate -format Literal -radix hexadecimal /single_cycle_core_testbench/uut/sig_MEM_RS
add wave -noupdate -format Literal -radix hexadecimal /single_cycle_core_testbench/uut/sig_MEM_RT
add wave -noupdate -format Literal -radix hexadecimal /single_cycle_core_testbench/uut/sig_MEM_RD
add wave -noupdate -format Literal -radix hexadecimal /single_cycle_core_testbench/uut/sig_MEM_read_b
add wave -noupdate -format Logic -radix hexadecimal /single_cycle_core_testbench/uut/sig_MEM_mem_write
add wave -noupdate -format Logic -radix hexadecimal /single_cycle_core_testbench/uut/sig_MEM_mem_to_reg
add wave -noupdate -format Logic -radix hexadecimal /single_cycle_core_testbench/uut/sig_MEM_reg_write
add wave -noupdate -format Literal -radix hexadecimal /single_cycle_core_testbench/uut/sig_MEM_write_reg
add wave -noupdate -format Literal -radix hexadecimal /single_cycle_core_testbench/uut/sig_MEM_alu_ctr
add wave -noupdate -format Literal -radix hexadecimal /single_cycle_core_testbench/uut/sig_MEM_alu_result
add wave -noupdate -format Literal -radix hexadecimal /single_cycle_core_testbench/uut/sig_MEM_data_out
add wave -noupdate -format Literal -radix hexadecimal /single_cycle_core_testbench/uut/sig_WB_RS
add wave -noupdate -format Literal -radix hexadecimal /single_cycle_core_testbench/uut/sig_WB_RT
add wave -noupdate -format Literal -radix hexadecimal /single_cycle_core_testbench/uut/sig_WB_RD
add wave -noupdate -format Logic -radix hexadecimal /single_cycle_core_testbench/uut/sig_WB_mem_write
add wave -noupdate -format Logic -radix hexadecimal /single_cycle_core_testbench/uut/sig_WB_mem_to_reg
add wave -noupdate -format Logic -radix hexadecimal /single_cycle_core_testbench/uut/sig_WB_reg_write
add wave -noupdate -format Literal -radix hexadecimal /single_cycle_core_testbench/uut/sig_WB_write_reg
add wave -noupdate -format Literal -radix hexadecimal /single_cycle_core_testbench/uut/sig_WB_alu_ctr
add wave -noupdate -format Literal -radix hexadecimal /single_cycle_core_testbench/uut/sig_WB_alu_result
add wave -noupdate -format Literal -radix hexadecimal /single_cycle_core_testbench/uut/sig_WB_data_out
add wave -noupdate -format Literal -radix hexadecimal /single_cycle_core_testbench/uut/sig_WB_write_data
add wave -noupdate -format Literal -radix hexadecimal -expand /single_cycle_core_testbench/uut/reg_file/sig_regfile
add wave -noupdate -format Literal -radix hexadecimal /single_cycle_core_testbench/uut/insn_mem/sig_insn_mem
add wave -noupdate -format Literal -radix hexadecimal -expand /single_cycle_core_testbench/signal_bus/shared_memory/sig_data_mem
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {0 ps} 0}
configure wave -namecolwidth 205
configure wave -valuecolwidth 40
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
update
WaveRestoreZoom {0 ps} {3633 ns}
