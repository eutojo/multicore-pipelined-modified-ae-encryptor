----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    09:18:49 04/27/2018 
-- Design Name: 
-- Module Name:    IF_ID_reg - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity IF_ID_reg3 is
	port(	clk		: in std_logic;
			stall	: in std_logic;
			cpu_stall : in std_logic;
			flush	: in std_logic;
			IF_pc	: in std_logic_vector(4 downto 0);
			IF_ins	: in std_logic_vector(15 downto 0);
			ID_pc	: buffer std_logic_vector(4 downto 0);
			ID_ins	: buffer std_logic_vector(15 downto 0));
end IF_ID_reg3;

architecture Behavioral of IF_ID_reg3 is
begin
	process
	begin
		wait until clk = '1' AND clk'EVENT;
			if flush = '0' then
				if stall = '1' then
					ID_pc <= ID_pc;
					ID_ins <= ID_ins;
				else
					ID_pc <= IF_pc;
					ID_ins <= IF_ins;
				end if;
			else
				ID_pc <= IF_pc;
				ID_ins <= "0000000000000000";
			end if;
	end process;

--	ID_pc <= IF_pc when (flush = '0' AND stall = '0') else
--				ID_pc when (flush = '0' AND stall = '1') else
--				IF_pc;
--				
--	ID_ins <= IF_ins when (flush = '0' AND stall = '0') else
--				ID_ins when (flush = '0') else
--				"0000000000000000";
				
end Behavioral;

