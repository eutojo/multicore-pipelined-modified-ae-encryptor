--------------------------------------------------------------------------------
-- Copyright (c) 1995-2003 Xilinx, Inc.
-- All Right Reserved.
--------------------------------------------------------------------------------
--   ____  ____ 
--  /   /\/   / 
-- /___/  \  /    Vendor: Xilinx 
-- \   \   \/     Version : 7.1.03i
--  \   \         Application : ISE WebPACK
--  /   /         Filename : single_cycle_core_testbench.vhw
-- /___/   /\     Timestamp : Tue Jul 25 16:23:28 2006
-- \   \  /  \ 
--  \___\/\___\ 
--
--Command: 
--Design Name: single_cycle_core_testbench
--Device: Xilinx
--

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
USE IEEE.STD_LOGIC_TEXTIO.ALL;
USE STD.TEXTIO.ALL;
use ieee.numeric_std.all ; 

ENTITY single_cycle_core_testbench IS
END single_cycle_core_testbench;

ARCHITECTURE testbench_arch OF single_cycle_core_testbench IS
    FILE RESULTS: TEXT OPEN WRITE_MODE IS "results.txt";

	COMPONENT data_bus
		port( reset : in std_logic;
		  clk : in std_logic;
		  fin_setup1	: in std_logic;
		  write_enable1 : in  std_logic;
          write_data1   : in  std_logic_vector(15 downto 0);
          addr_in1      : in  std_logic_vector(3 downto 0);
		  fin_setup2	: in std_logic;
		  write_enable2 : in  std_logic;
          write_data2   : in  std_logic_vector(15 downto 0);
          addr_in2      : in  std_logic_vector(3 downto 0);
		  fin_setup3	: in std_logic;
		  write_enable3 : in  std_logic;
          write_data3   : in  std_logic_vector(15 downto 0);
          addr_in3      : in  std_logic_vector(3 downto 0);
		  fin_setup4	: in std_logic;
		  write_enable4 : in  std_logic;
          write_data4   : in  std_logic_vector(15 downto 0);
          addr_in4      : in  std_logic_vector(3 downto 0);
          data_out     : out std_logic_vector(15 downto 0)
	);
	END COMPONENT;

    COMPONENT single_cycle_core
        port ( reset  : in  std_logic;
           clk    : in  std_logic;
           input_out : in std_logic_vector(7 downto 0);
		   ls_toggle : in std_logic;
           file_flag : out std_logic;
		   cpu_data_out: in std_logic_vector(15 downto 0);
		   cpu_write_enable: out std_logic;
		   cpu_write_data: out std_logic_vector(15 downto 0);
		   cpu_addr_in: out std_logic_vector(3 downto 0);
		   wait_signal: out std_logic;
		   ready_signal: out std_logic;
		   fin_setup: buffer std_logic;
		   fin_char : out std_logic;
		   not_fin_char: out std_logic;
		   cpu_stall: in std_logic;
		   mem_flag: out std_logic);
    END COMPONENT;
	
	COMPONENT single_cycle_core2
        port ( reset  : in  std_logic;
           clk    : in  std_logic;
		   cpu_data_out: in std_logic_vector(15 downto 0);
		   cpu_write_enable: out std_logic;
		   cpu_write_data: out std_logic_vector(15 downto 0);
		   cpu_addr_in: out std_logic_vector(3 downto 0);
		   wait_signal: out std_logic;
		   ready_signal: out std_logic;
		   fin_setup: buffer std_logic;
		   fin_char : out std_logic;
		   not_fin_char : out std_logic;
		   cpu_stall: in std_logic;
		   mem_flag: out std_logic);
    END COMPONENT;
	
	COMPONENT single_cycle_core3
        port ( reset  : in  std_logic;
           clk    : in  std_logic;
		   cpu_data_out: in std_logic_vector(15 downto 0);
		   cpu_write_enable: out std_logic;
		   cpu_write_data: out std_logic_vector(15 downto 0);
		   cpu_addr_in: out std_logic_vector(3 downto 0);
		   wait_signal: out std_logic;
		   ready_signal: out std_logic;
		   fin_setup: buffer std_logic;
		   fin_char : out std_logic;
		   cpu_stall: in std_logic;
		   mem_flag: out std_logic);
    END COMPONENT;

    SIGNAL reset : std_logic := '1';
    SIGNAL clk : std_logic := '0';
	 SIGNAL input_out : STD_LOGIC_VECTOR(7 downto 0) := "00000000";
	 SIGNAL file_flag : std_logic := '0';
	 SIGNAL ls_addr : std_logic_vector(3 downto 0) := "0000";
    SHARED VARIABLE TX_ERROR : INTEGER := 0;
    SHARED VARIABLE TX_OUT : LINE;

    constant PERIOD : time := 200 ns;
    constant DUTY_CYCLE : real := 0.5;
    constant OFFSET : time := 0 ns;
	
	SIGNAL data_out :std_logic_vector(15 downto 0);
	SIGNAL data_out_cpu1 :std_logic_vector(15 downto 0);
	
	SIGNAL addr_in1 : std_logic_vector(3 downto 0);
	SIGNAL write_enable1:std_logic;
	SIGNAL write_data1 : std_logic_vector(15 downto 0);
	
	SIGNAL addr_in2 : std_logic_vector(3 downto 0);
	SIGNAL write_enable2:std_logic;
	SIGNAL write_data2: std_logic_vector(15 downto 0);
	
	SIGNAL addr_in3 : std_logic_vector(3 downto 0);
	SIGNAL write_enable3:std_logic;
	SIGNAL write_data3 : std_logic_vector(15 downto 0);
	
	SIGNAL addr_in4 : std_logic_vector(3 downto 0);
	SIGNAL write_enable4:std_logic;
	SIGNAL write_data4 : std_logic_vector(15 downto 0);
	
	-- ## NOTE ##
	-- cpu_wait : waiting for a value from another cpu
	-- cpu_ready : data is available to pass onto another cpu
	-- cpu_stall : when the data isnt available yet
	-- fin_setup : using data memory
	
	SIGNAL cpu1_wait: std_logic;	
	SIGNAL cpu1_ready: std_logic;
	SIGNAL cpu1_stall: std_logic := '0';
	SIGNAL cpu1_fin_setup: std_logic := '0';
	SIGNAL sig_cpu1_fin_setup: std_logic;
	SIGNAL cpu1_not_fin_char: std_logic:= '0';
	SIGNAL sig_cpu1_not_fin_char: std_logic;
	
	SIGNAL cpu2_wait: std_logic;
	SIGNAL cpu2_ready: std_logic;
	SIGNAL cpu2_stall: std_logic;
	SIGNAL cpu2_fin_setup: std_logic := '0';
	SIGNAL sig_cpu2_fin_setup: std_logic;
	SIGNAL cpu2_not_fin_char: std_logic;

	SIGNAL cpu3_wait: std_logic := '1';
	SIGNAL cpu3_ready: std_logic;
	SIGNAL cpu3_stall: std_logic;
	SIGNAL cpu3_fin_setup: std_logic:= '0';
	SIGNAL sig_cpu3_fin_setup: std_logic:= '1';
	SIGNAL cpu3_not_fin_char: std_logic;
	
	SIGNAL cpu4_wait: std_logic := '1';
	SIGNAL cpu4_ready: std_logic;
	SIGNAL cpu4_stall: std_logic;
	SIGNAL cpu4_fin_setup: std_logic := '1';
		
	SIGNAL cpu1_access: std_logic;
	SIGNAL cpu2_access: std_logic;
	SIGNAL cpu3_access: std_logic;
	SIGNAL cpu4_access: std_logic;
	
	SIGNAL cpu1_mem_access: std_logic;	
	SIGNAL cpu1_mem: std_logic := '1';
	SIGNAL cpu2_mem: std_logic := '0';
	SIGNAL cpu3_mem: std_logic := '0';
	SIGNAL cpu4_mem: std_logic;
	
	SIGNAL cpu1_fin_char: std_logic;
	SIGNAL cpu2_fin_char: std_logic;
	SIGNAL cpu3_fin_char: std_logic;
	
	SIGNAL cpu1_char_buffer: std_logic_vector(3 downto 0) := "0000";
	SIGNAL cpu2_char_buffer: std_logic_vector(3 downto 0) := "0000";
	SIGNAL cpu3_char_buffer: std_logic_vector(3 downto 0) := "0000";
	
	SIGNAL cpu2_setup_flag: std_logic := '0';
	SIGNAL cpu3_setup_flag: std_logic := '0';
	
	SIGNAL cpu1_buffer_flag: std_logic;
	
	SIGNAL next_char: std_logic_vector(7 downto 0);
	
    BEGIN
			cpu4_access <= '1' when (cpu4_stall = '0' AND cpu4_fin_setup = '1') else '0';
		
		cpu1_mem_access <= '1' when (cpu1_mem = '1' AND cpu1_access = '1');
		
		signal_bus: data_bus
		PORT MAP(reset => reset,
		  clk => clk,
		  fin_setup1	=> cpu1_mem,
		  write_enable1 => write_enable1,
          write_data1   => write_data1,
          addr_in1      => addr_in1,
		  fin_setup2	=> cpu2_mem,
		  write_enable2 => write_enable2,
          write_data2   => write_data2,
          addr_in2      => addr_in2,
		  fin_setup3	=> cpu3_mem,
		  write_enable3 => write_enable3,
          write_data3   => write_data3,
          addr_in3      => addr_in3,
		  fin_setup4	=> cpu4_access,
		  write_enable4 => write_enable4,
          write_data4   => write_data4,
          addr_in4      => addr_in4,
          data_out     => data_out);
		
		-- #### CODE FOR WHEN EACH PROCESSOR HAS FINISHED SETTING UP
		-- code for CPU1 setup flag
		PROCESS(sig_cpu1_fin_setup)
		BEGIN
			IF rising_edge(sig_cpu1_fin_setup) THEN
				cpu1_fin_setup <= '1';
			END IF;
		END PROCESS;
		
		-- fin setup
		PROCESS(sig_cpu2_fin_setup)
		BEGIN
			IF rising_edge(sig_cpu2_fin_setup) THEN
				cpu2_fin_setup <= '1';
			END IF;
		END PROCESS;
		
		-- code for CPU3 setup flag
		PROCESS(sig_cpu3_fin_setup)
		BEGIN
			IF falling_edge(sig_cpu3_fin_setup) THEN-- AND cpu3_fin_setup = '0' THEN
				cpu3_fin_setup <= '1';
			END IF;
		END PROCESS;
		
		-- ## CODE TO TELL CPU1 WHETHER TO FETCH CHARACTER OR WAIT FOR CPU2
		
		-- code for CPU1 not fin char
		PROCESS(sig_cpu1_fin_setup, sig_cpu1_not_fin_char)
		BEGIN
			IF rising_edge(sig_cpu1_not_fin_char) THEN
				cpu1_not_fin_char <= '1';
			ELSIF rising_edge(sig_cpu1_fin_setup) AND sig_cpu1_not_fin_char = '0' THEN
				cpu1_not_fin_char <= '0';
			END IF;
		END PROCESS;
		
		-- ## CODE FOR STALLING ##
		-- stop stalling either:
		-- a) not waiting on cpu2 and cpu2 is not using memory
		-- b) cpu2 says its ready
		-- c) have a job and cpu2 isnt using memory
		-- extra conditions for core 1 in case it's waiting for no reason (needed to fix)
		
		-- code for CPU1 stall
		PROCESS(cpu3_ready, cpu1_wait, cpu3_fin_char, cpu3_mem, cpu2_mem, cpu3_fin_setup, cpu3_mem)
		BEGIN
			-- stall if cpu2 is using memory and you need it
			if (rising_edge(cpu1_wait)) THEN
				cpu1_stall <= '1';
			elsif cpu1_stall = '1' AND ((falling_edge(cpu2_mem) AND cpu3_fin_setup = '1' AND cpu3_char_buffer = "0000") OR rising_edge(cpu3_fin_char)
									OR (cpu2_fin_char = '1' AND cpu1_not_fin_char = '1' AND cpu3_char_buffer = "0000") OR 
									rising_edge(cpu3_fin_setup ) OR
									(falling_edge(cpu3_mem))) THEN
				cpu1_stall <= '0';
			else
				cpu1_stall <= cpu1_stall;
			END IF;
		END PROCESS;
		
		-- code for cpu2 stall
		PROCESS(cpu1_ready, cpu2_wait)
		BEGIN
			if (rising_edge(cpu2_wait)) THEN
				cpu2_stall <= '1';
			elsif cpu2_stall = '1' AND ((cpu2_char_buffer = "0000" AND rising_edge(cpu1_ready) AND cpu2_setup_flag = '0') OR 
												(cpu2_char_buffer /= "0000" AND cpu1_mem = '0' AND cpu3_mem = '0') OR
													(falling_edge(sig_cpu1_fin_setup) AND cpu2_char_buffer /= "0000")) THEN
				cpu2_stall <= '0';
			else
				cpu2_stall <= cpu2_stall;
			END IF;
		END PROCESS;
		
		-- cpu3 stall
		PROCESS(cpu2_ready, cpu3_wait, cpu2_fin_char, sig_cpu2_fin_setup)
		BEGIN
			if (rising_edge(cpu3_wait)) THEN
				cpu3_stall <= '1';
			elsif cpu3_stall = '1' AND ((cpu3_char_buffer = "0000" AND rising_edge(cpu2_ready) AND cpu3_fin_setup = '0') OR 
									(cpu3_char_buffer /= "0000" AND ((cpu1_mem = '0' AND cpu2_mem = '0') OR rising_edge(cpu2_ready))) OR
									(cpu3_char_buffer /= "0000" AND falling_edge(cpu2_fin_char))) THEN
				cpu3_stall <= '0';
			else
				cpu3_stall <= cpu3_stall;
			END IF;
		END PROCESS;
		
		-- ## CODE FOR BUFFER ##
		-- add to the next core's buffer when you've finished your job
		-- subtract from your own buffer when your job's done
		
		-- code for CPU1 buffer
		PROCESS(cpu2_fin_char, sig_cpu1_fin_setup)
		BEGIN
			IF cpu2_fin_char = '1' AND cpu2_not_fin_char = '1' THEN
				cpu1_char_buffer <= cpu1_char_buffer + 1;
			ELSIF rising_edge(sig_cpu1_fin_setup) AND cpu1_char_buffer /= "0000" THEN
				cpu1_char_buffer <= cpu1_char_buffer - 1;
			ELSE
				cpu1_char_buffer <= cpu1_char_buffer;
			END IF;
		END PROCESS;
		
		-- code for CPU2 buffer
		PROCESS(cpu1_fin_char, cpu2_fin_char)
		BEGIN
			IF cpu1_fin_char = '1' THEN
				cpu2_char_buffer <= cpu2_char_buffer + 1;
			ELSIF rising_edge(cpu2_fin_char) AND cpu2_char_buffer /= "0000" THEN
				cpu2_char_buffer <= cpu2_char_buffer - 1;
			ELSE
				cpu2_char_buffer <= cpu2_char_buffer;
			END IF;
		END PROCESS;
		
		-- code for CPU3 buffer
		PROCESS(cpu2_fin_char, cpu3_fin_char)
		BEGIN
			IF cpu2_fin_char = '1' AND cpu2_not_fin_char = '0' THEN
				cpu3_char_buffer <= cpu3_char_buffer + 1;
			ELSIF rising_edge(cpu3_fin_char) AND cpu3_char_buffer /= "0000" THEN
				cpu3_char_buffer <= cpu3_char_buffer - 1;
			ELSE
				cpu3_char_buffer <= cpu3_char_buffer;
			END IF;
		END PROCESS;
		
		
		-- ## CODE FOR MEMORY USAGE ##
		-- using memory after it has finished stalling (stall only occurs when it's waiting to access memory)
		-- stops using memory after it asserts ready.
		
		-- code for CPU1 memory usage
		PROCESS(cpu1_ready, cpu1_stall, cpu1_access)
		BEGIN
			IF falling_edge(cpu1_stall) AND cpu2_mem = '0' THEN
				cpu1_mem <= '1';
			ELSIF rising_edge(cpu1_ready) THEN
				cpu1_mem <= '0';
			END IF;
		END PROCESS;
		
		-- code for cpu2 memory usage
		PROCESS(cpu2_stall, cpu2_ready, cpu2_access)
		BEGIN
			IF falling_edge(cpu2_stall) THEN
				cpu2_mem <= '1';
			ELSIF rising_edge(cpu2_ready) THEN
				cpu2_mem <= '0';
			END IF;
		END PROCESS;
		
		-- code for cpu3 memory usage
		PROCESS(cpu3_stall, cpu3_ready)
		BEGIN
			IF falling_edge(cpu3_stall) THEN
				cpu3_mem <= '1';
			ELSIF rising_edge(cpu3_ready) THEN
				cpu3_mem <= '0';
			END IF;
		END PROCESS;
							
		UUT : single_cycle_core
        PORT MAP (reset  => reset,
           clk  => clk,
           input_out => input_out,
		   ls_toggle => cpu1_not_fin_char,
           file_flag => file_flag,
		   cpu_data_out => data_out,
		   cpu_write_enable => write_enable1,
		   cpu_write_data => write_data1,
		   cpu_addr_in => addr_in1,
		   wait_signal => cpu1_wait,
		   ready_signal => cpu1_ready,
		   fin_setup => sig_cpu1_fin_setup,
		   fin_char => cpu1_fin_char,
		   not_fin_char => sig_cpu1_not_fin_char,
		   cpu_stall => cpu1_stall,
		   mem_flag => cpu1_access
        );
		  
		  
		UUT2 : single_cycle_core2
        PORT MAP (reset  => reset,
           clk  => clk,
		   cpu_data_out => data_out,
		   cpu_write_enable => write_enable2,
		   cpu_write_data => write_data2,
		   cpu_addr_in => addr_in2,
		   wait_signal => cpu2_wait,
		   ready_signal => cpu2_ready,
		   fin_setup => sig_cpu2_fin_setup,
		   fin_char => cpu2_fin_char,
		   not_fin_char => cpu2_not_fin_char,
		   cpu_stall => cpu2_stall,
		   mem_flag => cpu2_access
        );

		UUT3 : single_cycle_core3
        PORT MAP (reset  => reset,
           clk  => clk,
		   cpu_data_out => data_out,
		   cpu_write_enable => write_enable3,
		   cpu_write_data => write_data3,
		   cpu_addr_in => addr_in3,
		   wait_signal => cpu3_wait,
		   ready_signal => cpu3_ready,
		   fin_setup => sig_cpu3_fin_setup,
		   fin_char => cpu3_fin_char,
		   cpu_stall => cpu3_stall,
		   mem_flag => cpu3_access
        );


        PROCESS    -- clock process for clk
        BEGIN
            WAIT for OFFSET;
            CLOCK_LOOP : LOOP
                clk <= '0';
                WAIT FOR (PERIOD - (PERIOD * DUTY_CYCLE));
                clk <= '1';
                WAIT FOR (PERIOD * DUTY_CYCLE);
            END LOOP CLOCK_LOOP;
        END PROCESS;

        PROCESS
            BEGIN
                -- -------------  Current Time:  285ns
                WAIT FOR 285 ns;
                reset <= '0';
                -- -------------------------------------
                WAIT FOR 100000 ns;--3175 ns;

                IF (TX_ERROR = 0) THEN
                    STD.TEXTIO.write(TX_OUT, string'("No errors or warnings"));
                    STD.TEXTIO.writeline(RESULTS, TX_OUT);
                    ASSERT (FALSE) REPORT
                      "Simulation successful (not a failure).  No problems detected."
                      SEVERITY FAILURE;
                ELSE
                    STD.TEXTIO.write(TX_OUT, TX_ERROR);
                    STD.TEXTIO.write(TX_OUT,
                        string'(" errors found in simulation"));
                    STD.TEXTIO.writeline(RESULTS, TX_OUT);
                    ASSERT (FALSE) REPORT "Errors found during simulation"
                         SEVERITY FAILURE;
                END IF;
            END PROCESS;
				

				-- read string to encrypt --
				PROCESS
            FILE INPUT_FILE: TEXT OPEN READ_MODE IS "input.txt";
            VARIABLE input_line : LINE;
            VARIABLE input_in : CHARACTER;
            
            BEGIN
	
				WAIT UNTIL file_flag = '1' AND file_flag'EVENT;
					IF ENDFILE(INPUT_FILE) THEN
						input_out <= "10000000";	-- EOF
					ELSIF cpu1_not_fin_char = '0' THEN
						READLINE(INPUT_FILE, input_line);
						READ(input_line, input_in);
						input_out <= std_logic_vector(to_unsigned(natural(character'pos(input_in)), 8));
					END IF;
					
            END PROCESS;



    END testbench_arch;

