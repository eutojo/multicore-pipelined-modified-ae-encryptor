----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    15:30:13 04/27/2018 
-- Design Name: 
-- Module Name:    comparator - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity comparator3 is
	port(branch_type	: in std_logic_vector(1 downto 0);
		data_a	: in std_logic_vector(15 downto 0);
		 data_b	: in std_logic_vector(15 downto 0);
		 equals	: out std_logic);
end comparator3;

architecture Behavioral of comparator3 is
	signal result: std_logic_vector(15 downto 0);
	constant OP_BNE   : std_logic_vector(3 downto 0) := "1111"; -- 15 F
	constant OP_BEQ   : std_logic_vector(3 downto 0) := "0010"; -- 2
begin

	result <= data_a - data_b;
	equals <= '1' when (result = "0000000000000000" and branch_type = "01")  else
			  '1' when (result /= "0000000000000000" and branch_type = "11") else
			  '1' when branch_type = "10" else
			  '0';

end Behavioral;

