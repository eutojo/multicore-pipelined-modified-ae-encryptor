----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    11:40:36 04/27/2018 
-- Design Name: 
-- Module Name:    MEM_WB_reg - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity MEM_WB_reg2 is
	port( 	clk					: in std_logic;
			cpu_stall			: in std_logic;
			MEM_mem_write		: in std_logic;
			MEM_mem_to_reg		: in std_logic;
			MEM_reg_write		: in std_logic;
			MEM_write_reg		: in std_logic_vector(3 downto 0);
			MEM_alu_ctr			: in std_logic_vector(2 downto 0);
			MEM_alu_result		: in std_logic_vector(15 downto 0);
			MEM_data_out		: in std_logic_vector(15 downto 0);
			MEM_RS 				: in std_logic_vector(3 downto 0);
			MEM_RT				: in std_logic_vector(3 downto 0);
			MEM_RD				: in std_logic_vector(3 downto 0);
			WB_mem_write		: buffer std_logic;
			WB_mem_to_reg		: buffer std_logic;
			WB_reg_write		:	buffer std_logic;
			WB_write_reg		: buffer std_logic_vector(3 downto 0);
			WB_alu_ctr			: buffer std_logic_vector(2 downto 0);
			WB_alu_result		: buffer std_logic_vector(15 downto 0);
			WB_data_out			: buffer std_logic_vector(15 downto 0);
			WB_RS 				: buffer std_logic_vector(3 downto 0);
			WB_RT				: buffer std_logic_vector(3 downto 0);
			WB_RD				: buffer std_logic_vector(3 downto 0));
end MEM_WB_reg2;

architecture Behavioral of MEM_WB_reg2 is

begin
	process
	begin
		wait until clk = '1' and clk'EVENT;
		
--			IF cpu_stall = '0' THEN
				WB_mem_to_reg 	<= MEM_mem_to_reg;
				WB_write_reg	<= MEM_write_reg;
				WB_alu_result	<= MEM_alu_result;
				WB_data_out		<= MEM_data_out;
				WB_RS			<= MEM_RS;
				WB_RT			<= MEM_RT;
				WB_RD			<= MEM_RD;
				WB_alu_ctr		<= MEM_alu_ctr;
				

				WB_mem_write 	<= MEM_mem_write;
				WB_reg_write	<= MEM_reg_write;
				--END IF;
--			ELSE
--				WB_mem_write 	<= WB_mem_write;
--				WB_mem_to_reg 	<= WB_mem_to_reg;
--				WB_reg_write	<= WB_reg_write;
--				WB_write_reg	<= WB_write_reg;
--				WB_alu_result	<= WB_alu_result;
--				WB_data_out		<= WB_data_out;
--				WB_RS			<= WB_RS;
--				WB_RT			<= WB_RT;
--				WB_RD			<= WB_RD;
--				WB_alu_ctr		<= WB_alu_ctr;
--			END IF;
	end process;

end Behavioral;

