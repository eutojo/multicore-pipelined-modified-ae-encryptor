---------------------------------------------------------------------------
-- instruction_memory.vhd - Implementation of A Single-Port, 16 x 16-bit
--                          Instruction Memory.
-- 
-- Notes: refer to headers in single_cycle_core.vhd for the supported ISA.
--
-- Copyright (C) 2006 by Lih Wen Koh (lwkoh@cse.unsw.edu.au)
-- All Rights Reserved. 
--
-- The single-cycle processor core is provided AS IS, with no warranty of 
-- any kind, express or implied. The user of the program accepts full 
-- responsibility for the application of the program and the use of any 
-- results. This work may be downloaded, compiled, executed, copied, and 
-- modified solely for nonprofit, educational, noncommercial research, and 
-- noncommercial scholarship purposes provided that this notice in its 
-- entirety accompanies all copies. Copies of the modified software can be 
-- delivered to persons who use it solely for nonprofit, educational, 
-- noncommercial research, and noncommercial scholarship purposes provided 
-- that this notice in its entirety accompanies all copies.
--
---------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity instruction_memory2 is
    port ( reset    : in  std_logic;
           clk      : in  std_logic;
           addr_in  : in  std_logic_vector(4 downto 0);
           insn_out : out std_logic_vector(15 downto 0) );
end instruction_memory2;

architecture behavioral of instruction_memory2 is

type mem_array is array(0 to 31) of std_logic_vector(15 downto 0);
signal sig_insn_mem : mem_array;

begin
    mem_process: process ( clk,
                           addr_in ) is
  
    variable var_insn_mem : mem_array;
    variable var_addr     : integer;
  
    begin
        if (reset = '1') then
				var_insn_mem(0)  := X"4000"; -- wait
				var_insn_mem(1)  := X"1012"; -- load 9ABC into $1
				var_insn_mem(2)  := X"1023"; -- load DEF0 into $2
				var_insn_mem(3)  := X"1034"; -- load 0x7F into $3  
				var_insn_mem(4)  := X"1045"; -- load EOF into $4
				var_insn_mem(5)  := X"5100"; -- load 2 into $5
				var_insn_mem(6)  := X"4000"; -- next cpu can start setting up
				var_insn_mem(7)  := X"1060"; 
				var_insn_mem(8)  := X"107F"; -- fetch character from buffer
				var_insn_mem(9)  := X"5000"; -- fetch m 
				var_insn_mem(10)  := X"F46C"; -- next person can use memory
				var_insn_mem(11)  := X"7019"; -- check end of file
				var_insn_mem(12)  := X"E616"; -- jmp if end of file
				var_insn_mem(13)  := X"9727"; -- first xor
				var_insn_mem(14)  := X"E626"; -- sub
				var_insn_mem(15)  := X"9727"; -- second xor
				var_insn_mem(16)  := X"B636"; -- sub
				var_insn_mem(17)  := X"4000"; -- and
				var_insn_mem(18)  := X"3061"; -- wait for memory
				var_insn_mem(19)  := X"307F"; -- store character
				var_insn_mem(20)  := X"5001"; -- store m
				var_insn_mem(21)  := X"7006"; -- finished CPU2's job
				var_insn_mem(22)  := X"0000"; -- start next thing
				var_insn_mem(23)  := X"0000";
				var_insn_mem(24)  := X"0000";
				var_insn_mem(25)  := X"0000"; 
				var_insn_mem(26)  := X"0000";
				var_insn_mem(27)  := X"0000";
				var_insn_mem(28)  := X"0000";
				var_insn_mem(29)  := X"0000";
				var_insn_mem(30)  := X"0000";
				var_insn_mem(31)  := X"0000";

			
--				---------------------------------------
--				-------------------------------------------
--	
--				 --load the encryption key into the registers
--				var_insn_mem(0)  := X"1010"; -- $1 <= 1234
--				var_insn_mem(1)  := X"1021"; -- $2 <= 5678
--            	var_insn_mem(2)  := X"1032"; -- $3 <= 9ABC
--            	var_insn_mem(3)  := X"1043"; -- $4 <= DEF0
--				
--				-- load the last AND to ensure that the string is printable
--            	var_insn_mem(4)  := X"1054"; -- $5 <= 0x7F
--				
--				-- load in for EOF comparison
--				var_insn_mem(5)  := X"1065"; -- $6 <= "x80"
--				
--				-- loading the tag gen key
--				var_insn_mem(6)  := X"10C0"; -- $12 <= tag gen key
--				var_insn_mem(7) := X"10D1"; -- $13
--				var_insn_mem(8) := X"10E2"; -- $14
--				var_insn_mem(9) := X"10F3"; -- $15 <= tag gen key
--				
--				-- character encryption
--				var_insn_mem(10)  := X"D088"; -- $8 <= character to encrypt
--				var_insn_mem(11)  := X"F86D"; -- if not EOF, go to 10
--				var_insn_mem(12)  := X"7022"; -- jump to the end
--				var_insn_mem(13) := X"E818"; -- 8 XOR 1234
--				var_insn_mem(14) := X"E828"; -- 8 XOR 5678

--				-- ###
--				var_insn_mem(15) := X"E838"; -- 8 XOR 9ABC
--				var_insn_mem(16) := X"E848"; -- 8 XOR DEF0
--				var_insn_mem(17) := X"B588"; -- the last AND				
--				-- tag generation
--				var_insn_mem(18) := X"6C9F"; -- get the MSB off $12 and into $9
--				var_insn_mem(19) := X"C89B"; -- shift encrypted according to $9 and store in $11				
--				var_insn_mem(20) := X"EBAA"; -- XOR $11 with $10

--				-- ###
--								-- shifting the key
--				var_insn_mem(21) := X"6E7F"; -- store LSB of $14 in 7
--				var_insn_mem(22) := X"6F9F"; -- store LSB of $15 in 8
--				var_insn_mem(23) := X"5FF1"; -- shift $15 by 1
--				var_insn_mem(24) := X"8FF7"; -- get the shifted value of $15
--				var_insn_mem(25) := X"6D7F"; -- get LSB of $13 in 7
--				var_insn_mem(26) := X"5EE1"; -- shift $14 by 1
--				-- ###
--				var_insn_mem(27) := X"8EE7"; -- get the shifted value of $14
--				var_insn_mem(28) := X"6C7F"; -- store LSB of $12 in 7
--				var_insn_mem(29) := X"5DD1"; -- shift $13 by 1
--				var_insn_mem(30) := X"8DD7"; -- get the shifted value of $13
--				var_insn_mem(31) := X"5CC1"; -- shift 12 by 1
--				var_insn_mem(32) := X"8CC9"; -- get the shifted value of $12
--				
--				-- get the next character
--				var_insn_mem(33)  := X"700A"; -- jump to load the next character
--				
--				-- finished
--            	var_insn_mem(34)  := X"38A0"; -- saving the results of tag gen
--            	var_insn_mem(35)  := X"0000"; -- blank
--            	var_insn_mem(36)  := X"7023"; -- jump blank
--				var_insn_mem(37)  := X"0000"; 
--				
--				var_insn_mem(39)  := X"0000"; 
--				var_insn_mem(40) := X"0000"; 
--				var_insn_mem(41) := X"0000"; 
--				var_insn_mem(42) := X"0000"; 
--				var_insn_mem(43)  := X"0000"; 
--				var_insn_mem(44)  := X"0000"; 
--				var_insn_mem(45)  := X"0000"; 
--				var_insn_mem(46) := X"0000";
--				var_insn_mem(47) := X"0000"; 
--				var_insn_mem(48) := X"0000";
--				var_insn_mem(49) := X"0000";
--				var_insn_mem(50) := X"0000";
--				var_insn_mem(51) := X"0000";
--				var_insn_mem(52) := X"0000";				
--				var_insn_mem(53) := X"0000";
--				var_insn_mem(54) := X"0000";
--				var_insn_mem(55) := X"0000"; 
--				var_insn_mem(56) := X"0000"; 
--				var_insn_mem(57) := X"0000"; 
--				var_insn_mem(58) := X"0000"; 
--				var_insn_mem(59) := X"0000";   
--				var_insn_mem(60) := X"0000"; 
--				var_insn_mem(61) := X"0000";
--				var_insn_mem(62) := X"0000";
--				var_insn_mem(63) := X"0000";
				
				

        
        elsif (rising_edge(clk)) then
            -- read instructions on the rising clock edge
            var_addr := conv_integer(addr_in);
            insn_out <= var_insn_mem(var_addr);
        end if;

        -- the following are probe signals (for simulation purpose)
        sig_insn_mem <= var_insn_mem;

    end process;
  
end behavioral;
