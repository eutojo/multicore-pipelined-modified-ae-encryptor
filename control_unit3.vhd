---------------------------------------------------------------------------
-- control_unit.vhd - Control Unit Implementation
--
-- Notes: refer to headers in single_cycle_core.vhd for the supported ISA.
--
--  control signals:
--     reg_dst    : asserted for ADD instructions, so that the register
--                  destination number for the 'write_register' comes from
--                  the rd field (bits 3-0). 
--     reg_write  : asserted for ADD and LOAD instructions, so that the
--                  register on the 'write_register' input is written with
--                  the value on the 'write_data' port.
--     alu_src    : asserted for LOAD and STORE instructions, so that the
--                  second ALU operand is the sign-extended, lower 4 bits
--                  of the instruction.
--     mem_write  : asserted for STORE instructions, so that the data 
--                  memory contents designated by the address input are
--                  replaced by the value on the 'write_data' input.
--     mem_to_reg : asserted for LOAD instructions, so that the value fed
--                  to the register 'write_data' input comes from the
--                  data memory.
--
--
-- Copyright (C) 2006 by Lih Wen Koh (lwkoh@cse.unsw.edu.au)
-- All Rights Reserved. 
--
-- The single-cycle processor core is provided AS IS, with no warranty of 
-- any kind, express or implied. The user of the program accepts full 
-- responsibility for the application of the program and the use of any 
-- results. This work may be downloaded, compiled, executed, copied, and 
-- modified solely for nonprofit, educational, noncommercial research, and 
-- noncommercial scholarship purposes provided that this notice in its 
-- entirety accompanies all copies. Copies of the modified software can be 
-- delivered to persons who use it solely for nonprofit, educational, 
-- noncommercial research, and noncommercial scholarship purposes provided 
-- that this notice in its entirety accompanies all copies.
--
---------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity control_unit3 is
    port ( opcode     : in  std_logic_vector(3 downto 0);
           reg_dst    : out std_logic;
           reg_write  : out std_logic;
           alu_src    : out std_logic;
           mem_write  : out std_logic;
           mem_to_reg : out std_logic;
		   alu_ctr	 : out std_logic_vector(2 downto 0);
		   npc_sel	 : out std_logic_vector(1 downto 0));
end control_unit3;

architecture behavioural of control_unit3 is

constant OP_LOAD  : std_logic_vector(3 downto 0) := "0001"; -- 1
constant OP_STORE : std_logic_vector(3 downto 0) := "0011"; -- 3
constant OP_BEQ   : std_logic_vector(3 downto 0) := "0010"; -- 2
-- 4 reserved for waiting
constant OP_READY  : std_logic_vector(3 downto 0) := "0101"; -- 5
constant OP_SRL	: std_logic_vector(3 downto 0) := "0110"; -- 6
constant OP_JMP	: std_logic_vector(3 downto 0) := "0111"; -- 7
constant OP_ADD   : std_logic_vector(3 downto 0) := "1000"; -- 8
constant OP_NEXTCHAR   : std_logic_vector(3 downto 0) := "1001"; -- 8
constant OP_SLL	: std_logic_vector(3 downto 0) := "1010"; -- 10
constant OP_SLLV	: std_logic_vector(3 downto 0) := "1011"; -- 11 --B
constant OP_XOR	: std_logic_vector(3 downto 0) := "1110"; -- 14 E
constant OP_BNE   : std_logic_vector(3 downto 0) := "1111"; -- 15 F

begin

   reg_dst    <= '1' when (opcode = OP_ADD
									 or opcode = OP_SLLV
									 or opcode = OP_SLL
									 or opcode = OP_SRL
									 or opcode = OP_XOR) else
                  '0';

    reg_write  <= '1' when (opcode = OP_ADD
									 or opcode = OP_LOAD
									 or opcode = OP_SLLV
									 or opcode = OP_SRL
									 or opcode = OP_SLL
									 or opcode = OP_XOR
									 or opcode = OP_NEXTCHAR) else
                  '0';
    
    alu_src    <= '1' when (opcode = OP_LOAD 
                           or opcode = OP_STORE 
						   or opcode = OP_SLLV) else
                  '0';
						
	 alu_ctr 	<= 	"001" when (opcode = OP_SLL OR opcode = OP_SLLV) else --1
					"010" when (opcode = OP_ADD)else --2
					"011" when (opcode = OP_XOR) else --3
					"100" when (opcode = OP_SRL) else --4
					"101" when (opcode = OP_BEQ) else --5
					"110" when (opcode = OP_READY) else -- 6
					"000";
                 
    mem_write  <= '1' when (opcode = OP_STORE) else
                  '0';
                 
    mem_to_reg <= '1' when opcode = OP_LOAD else
                  '0';
						
	 npc_sel		<= 	"01" when (opcode = OP_BEQ) else
						"11" when (opcode = OP_BNE) else
						"10" when (opcode = OP_JMP) else
						"00";

end behavioural;
