----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    10:20:14 04/27/2018 
-- Design Name: 
-- Module Name:    EX_MEM_reg - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity EX_MEM_reg2 is
	port(	clk					: in std_logic;
			flush					: in std_logic;
			cpu_stall			: in std_logic;
			EX_read_b			: in std_logic_vector(15 downto 0);
			EX_mem_write		: in std_logic;
			EX_mem_to_reg		: in std_logic;
			EX_reg_write		: in std_logic;
			EX_write_reg		: in std_logic_vector(3 downto 0);
			EX_alu_ctr			: in std_logic_vector(2 downto 0);
			EX_alu_result		: in std_logic_vector(15 downto 0);
			EX_RS 				: in std_logic_vector(3 downto 0);
			EX_RT				: in std_logic_vector(3 downto 0);
			EX_RD				: in std_logic_vector(3 downto 0);
			MEM_read_b			: buffer std_logic_vector(15 downto 0);
			MEM_mem_write		: buffer std_logic;
			MEM_mem_to_reg		: buffer std_logic;
			MEM_reg_write		: buffer std_logic;
			MEM_write_reg		: buffer std_logic_vector(3 downto 0);
			MEM_alu_ctr			: buffer std_logic_vector(2 downto 0);
			MEM_alu_result		: buffer std_logic_vector(15 downto 0);
			MEM_RS 				: buffer std_logic_vector(3 downto 0);
			MEM_RT				: buffer std_logic_vector(3 downto 0);
			MEM_RD				: buffer std_logic_vector(3 downto 0));
end EX_MEM_reg2;

architecture Behavioral of EX_MEM_reg2 is
begin
	
	process
	begin
		wait until clk = '1' and clk'EVENT;
--			IF cpu_stall = '0' THEN
				MEM_read_b 		<= EX_read_b;
				MEM_mem_to_reg 	<= EX_mem_to_reg;
				MEM_write_reg	<= EX_write_reg;
				MEM_alu_result	<= EX_alu_result;
				MEM_RS			<= EX_RS;
				MEM_RT			<= EX_RT;
				MEM_RD			<= EX_RD;
				MEM_alu_ctr		<= EX_alu_ctr;				
				if flush = '1' then
					MEM_reg_write	<= '0';
					MEM_mem_write 	<= '0';
					MEM_alu_ctr		<= "000";				
				else
					MEM_reg_write	<= EX_reg_write;
					MEM_mem_write 	<= EX_mem_write;
					MEM_alu_ctr		<= EX_alu_ctr;				
				end if;
--			ELSE
--				MEM_read_b 		<= MEM_read_b;
--				MEM_mem_to_reg 	<= MEM_mem_to_reg;
--				MEM_write_reg	<= MEM_write_reg;
--				MEM_alu_result	<= MEM_alu_result;
--				MEM_RS			<= MEM_RS;
--				MEM_RT			<= MEM_RT;
--				MEM_RD			<= MEM_RD;
--				MEM_reg_write	<= MEM_reg_write;
--				MEM_mem_write 	<= MEM_mem_write;
--				MEM_alu_ctr		<= MEM_alu_ctr;
--			END IF;
	end process;
end Behavioral;

